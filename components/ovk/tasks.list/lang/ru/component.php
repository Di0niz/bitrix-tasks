<?
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "Модуль управления задачами не доступен в данной редакции продукта.";
$MESS["TASKS_MODULE_NOT_FOUND"] = "Модуль управления задачами не установлен.";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Модуль \"Социальная сеть\" не установлен";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "Модуль форум не установлен";
$MESS["TASKS_TITLE_TASKS"] = "Задачи";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Задачи группы";
$MESS["TASKS_TITLE_MY_TASKS"] = "Мои задачи";
?>