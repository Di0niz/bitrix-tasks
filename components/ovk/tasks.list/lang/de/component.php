<?
$MESS["TASKS_MODULE_NOT_FOUND"] = "Das Modul Aufgaben ist nicht installiert.";
$MESS["TASKS_TITLE_TASKS"] = "Aufgaben";
$MESS["TASKS_TITLE_MY_TASKS"] = "Meine Aufgaben";
$MESS["SOCNET_MODULE_NOT_INSTALLED"] = "Das Modul \"Soziales Netzwerk\" ist nicht installiert.";
$MESS["FORUM_MODULE_NOT_INSTALLED"] = "Das Modul Forum ist nicht installiert.";
$MESS["TASKS_TITLE_GROUP_TASKS"] = "Aufgaben der Gruppe";
$MESS["TASKS_MODULE_NOT_AVAILABLE_IN_THIS_EDITION"] = "Das Modul Aufgaben ist in dieser Produktedition nicht verfügbar.";
?>